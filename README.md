# Genesys Channel Driver for TwilioSMS #

This is a Genesys Channel Driver for Social Messaging Server that allows for integration to Twilio SMS.  This channel driver will allow agents to handle SMS received via Twilio and respond with SMS to be sent by Twilio.

For a walk-thru of the code and how to set it up in a Genesys environment, check out: [Twilio SMS with Social Messaging Server](https://developer.genesys.com/2017/02/13/twilio-sms-with-social-messaging-server/)