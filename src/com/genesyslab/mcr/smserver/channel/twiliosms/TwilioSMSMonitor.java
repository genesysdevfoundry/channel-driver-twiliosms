/**
 * File:    TwilioSMSMonitor.java
 * Project: Twilio SMS Channel Sample
 */
package com.genesyslab.mcr.smserver.channel.twiliosms;

import com.genesyslab.mcr.smserver.channel.ChannelDriverConstants;
import com.genesyslab.mcr.smserver.gframework.FieldNames;
import com.genesyslab.mcr.smserver.gframework.LmsMessages;
import com.genesyslab.platform.commons.collections.KeyValueCollection;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import com.twilio.Twilio;
import com.twilio.type.PhoneNumber;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;

public class TwilioSMSMonitor
        implements HttpHandler
{
    private TwilioSMSDriver driver;
    private TwilioSMSDriverParams driverParams = null;
    private HttpServer httpServer;

    /**
     * @param driver
     */
    public TwilioSMSMonitor(TwilioSMSDriver driver)
    {
        this.driver = driver;
        this.driverParams = driver.driverParams;

        final String logRpfx = driver.logPfx("TwilioSMSMonitor.<init>");
        driver.getGfrPU().trace(LmsMessages.called, logRpfx, "");
    }

    public void start() throws Exception {
        final String logRpfx = driver.logPfx("TwilioSMSMonitor.start");
        driver.getGfrPU().trace(LmsMessages.called, logRpfx, "");

        httpServer = HttpServer.create(new InetSocketAddress(driverParams.monitorListeningPort), 0);
        httpServer.createContext("/" + driverParams.monitorContextURI, this)
                .getFilters().add(new ParametersFilter());
        httpServer.setExecutor(null); // creates a default executor
        httpServer.start();

        driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                "HTTP Server listening on port " + driverParams.monitorListeningPort + " for context /" + driverParams.monitorContextURI);
    }

    public void stop() throws Exception {
        final String logRpfx = driver.logPfx("TwilioSMSMonitor.stop");
        driver.getGfrPU().trace(LmsMessages.called, logRpfx, "");

        httpServer.stop(0);
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        final String logRpfx = driver.logPfx("handle");
        driver.getGfrPU().trace(LmsMessages.called, logRpfx, "");

        String sessionId = getSessionId(exchange);

        try {
            KeyValueCollection messageData = new KeyValueCollection();

            Map<String, Object> postParams =
                    (Map<String, Object>)exchange.getAttribute("parameters");

            driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                    "Building message data");

            // SM Server's required fields =====================================================
            messageData.addString(FieldNames.UMS_Channel, driver.channelPU.getChannelName());
            messageData.addString(FieldNames.UMS_MsgContext, (String)("" + postParams.get("From") + postParams.get("To")));

            if ( postParams.containsKey("MessageSid") ) {
                driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                        "Populating MessageSid fields");
                messageData.addString(ChannelDriverConstants.UMSKEY_MsgId, postParams.get("MessageSid").toString());
            }

            if ( postParams.containsKey("From") ) {
                driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                        "Populating From fields");
                messageData.addString(ChannelDriverConstants.UMSKEY_FromAddr, postParams.get("From").toString());
                messageData.addString(ChannelDriverConstants.UMSKEY_PhoneNumber, postParams.get("From").toString());
                messageData.addString(FieldNames.UMS_FromAddr, postParams.get("From").toString());
                messageData.addString("_smsSrcNumber", postParams.get("From").toString());
            }

            if ( postParams.containsKey("To") ) {
                driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                        "Populating To fields");
                messageData.addString(ChannelDriverConstants.UMSKEY_ToAddr, postParams.get("To").toString());
                messageData.addString(FieldNames.UMS_ToAddr, postParams.get("To").toString());
                messageData.addString("_smsText", postParams.get("Body").toString());
            }

            if ( postParams.containsKey("Body") ) {
                driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                        "Populating Body fields");
                messageData.addUTFString(ChannelDriverConstants.UMSKEY_MsgPlainText, postParams.get("Body").toString());
                messageData.addString(FieldNames.UMS_MsgPlainText, postParams.get("Body").toString());
            }

//            messageData.addString(FieldNames.UMS_MediaType, "sms");
//            messageData.addString(FieldNames.UMS_MediaTypeChat, "smssession");
            messageData.addString(FieldNames.UMS_MediaType, driverParams.mediaType);
            messageData.addString(FieldNames.UMS_MediaTypeChat, driverParams.mediaTypeChat);
            messageData.addString(FieldNames.UMS_MsgType, "regular");
            messageData.addString(FieldNames.UMS_ChatPossible, "true");
            messageData.addString(FieldNames.UMS_ChatRequired, "true");

            // =================================================================================

            driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                    "data from sample...\n" + messageData.toStringLine());

            // Submit message ============================================
            driver.channelPU.submitMessage(postParams.get("MessageSid").toString(), messageData);
            // ===========================================================
        } catch(Exception exc) {
            driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                    "Exception processing Twiml: " + exc.getMessage());

        }

        String response = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><Response></Response>";
        byte[] responseBody = response.getBytes();

        Headers responseHeaders = exchange.getResponseHeaders();
        responseHeaders.set("Content-Type", "text/xml");
        if ( sessionId != null ) {
            responseHeaders.set("Set-Cookie", "SID=" + sessionId + ";Path=/;HttpOnly");
        }
        exchange.sendResponseHeaders(200, responseBody.length);
        OutputStream os = exchange.getResponseBody();
        os.write(responseBody);
        os.close();
        exchange.close();

        driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                "Responded to Twiml: " + response);
    }

    public void send(String toNumber, String fromNumber, String msgText) {
        final String logRpfx = driver.logPfx("send");

        Twilio.init(driverParams.twilioAccountSID, driverParams.twilioAuthToken);

        com.twilio.rest.api.v2010.account.Message message = com.twilio.rest.api.v2010.account.Message
                .creator(new PhoneNumber( toNumber), new PhoneNumber(fromNumber), msgText)
                .create();

        driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                "Send SID: " + message.getSid());

    }

    public String getSessionId(HttpExchange exchange) {
        final String logRpfx = driver.logPfx("getSessionId");

        try {
            List<String> cookies = exchange.getRequestHeaders().get("Set-Cookie");
            for (String cookie : cookies) {
                if (cookie.startsWith("SID=")) {
                    String[] items = cookie.split(",");
                    return items[0].substring(4);
                }
            }
        } catch(Exception exc) {
            driver.getGfrPU().trace(LmsMessages.generic_trc2, logRpfx,
                    "Error getting session id: " + exc.toString());
        }

        return null;
    }
}

