/**
 * File:    TwilioSMSDriverParams.java
 * Project: Twilio SMS Channel Sample
 */
package com.genesyslab.mcr.smserver.channel.twiliosms;

import com.genesyslab.mcr.smserver.gframework.CfgOptions;
import com.genesyslab.mcr.smserver.gframework.LmsMessages;

public class TwilioSMSDriverParams
{
     // Configuration properties exposed to other classes.
    protected String twilioAccountSID;
    protected String twilioAuthToken;
    protected int monitorListeningPort;
    protected String monitorContextURI;
    protected String mediaType;
    protected String mediaTypeChat;

    // Names of configuration options

    // The media type for the interaction in page mode.  Should be 'sms'
    private final static String OPTION_MEDIA_TYPE ="media-type";

    // The media type for the interaction if treated as a chat in session mode.  Should be 'smssession'.
    private final static String OPTION_MEDIA_TYPE_CHAT = "media-type-chat";

    // The value of your Twilio Account SID
    private final static String OPTION_TWILIO_ACCOUNT_SID = "twilio-account-sid";

    // The value of your Twilio Auth Token
    private final static String OPTION_TWILIO_AUTH_TOKEN = "twilio-auth-token";

    // The port that the monitor's HTTP listener will be listening on
    private final static String OPTION_LISTENING_PORT = "listening-port";

    // The context URI for the HTTP listener.  Should be 'twiml'
    private final static String OPTION_CONTEXT_URI = "context-uri";

    // local
    private TwilioSMSDriver driver;

    /**
     * Constructor
     */
    public TwilioSMSDriverParams(TwilioSMSDriver driver) {

        this.driver = driver;
    }

    /**
     * Fetch configuration of the driver.
     *
     * @throws Exception
     *             if failed to get configuration options
     */
    protected void getConfiguration()
            throws Exception
    {
        final String logRpfx = driver.logPfx("getConfiguration");

        String channelSection = driver.channelPU.getChannelName();

        // get data fetching monitor's option MOPTION_twilio_account_sid
        mediaType = CfgOptions.getOption(channelSection, OPTION_MEDIA_TYPE, null,
                null, null, logRpfx, true);

        // get data fetching monitor's option MOPTION_twilio_account_sid
        mediaTypeChat = CfgOptions.getOption(channelSection, OPTION_MEDIA_TYPE_CHAT, null,
                null, null, logRpfx, true);

        // get data fetching monitor's option MOPTION_twilio_account_sid
        twilioAccountSID = CfgOptions.getOption(channelSection, OPTION_TWILIO_ACCOUNT_SID, null,
                null, null, logRpfx, true);

        // get data fetching monitor's option MOPTION_twilio_account_sid
        twilioAuthToken = CfgOptions.getOption(channelSection, OPTION_TWILIO_AUTH_TOKEN, null,
                null, null, logRpfx, true);

        // get data fetching monitor's option MOPTION_sampling_period
        monitorListeningPort = CfgOptions.getIntOption(channelSection, OPTION_LISTENING_PORT, new int[] { 0, -3600 },
                8000, null, logRpfx, false);

        // get data fetching monitor's option MOPTION_twilio_account_sid
        monitorContextURI = CfgOptions.getOption(channelSection, OPTION_CONTEXT_URI, null,
                "twiml", null, logRpfx, true);

        driver.getGfrPU().trace(LmsMessages.generic_std2, logRpfx, "configuration processing finished");
    }
}
